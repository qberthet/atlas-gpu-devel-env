# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for CUDA 12.3.
#

export CUDA_PATH=/usr/local/cuda-12.3
export PATH=${CUDA_PATH}/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=${CUDA_PATH}/lib64:${CUDA_PATH}/extras/CUPTI/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export CUDACXX=${CUDA_PATH}/bin/nvcc
export CUDAFLAGS="-allow-unsupported-compiler"
