# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for oneAPI.
#

# Use the Intel script to set up the environment.
source /opt/intel/oneapi/setvars.sh --include-intel-llvm

# Set up environment variables for using the oneAPI compiler with CMake.
# By default conly building code for "Intel backends", as building for more
# backends in parallel can be a fragile setup.
export CC=`which clang`
export CXX=`which clang++`
export CUDAHOSTCXX="${CXX}"
export SYCLCXX="${CXX} -fsycl"
export SYCLFLAGS="-fsycl-targets=spir64,spir64_x86_64"
