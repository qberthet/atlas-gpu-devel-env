# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Settings for the ATLAS software development runtime environment.
#

# Definition of the setupATLAS command.
setupATLAS() {
   export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
   source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
}
