# Derived Docker Images

This directory holds the configurations for images that take the "base" image
of the repository, and install some additional components on top of it. These
are:
  - `fullcuda`: Installs the **full** CUDA development environment in the image,
    including all of the machine learning, linear algebra, etc. development
    libraries.
