# ATLAS GPU Development Environment

This repository holds some code for building a
[Docker](https://www.docker.com/) image that could then be used to develop
"GPU code" for ATLAS, on top of the
[atlas/athena](https://gitlab.cern.ch/atlas/athena) software project(s).

## Image Contents

The image comes with the following packaged inside of it:
  - [GCC 11.3](https://gcc.gnu.org/onlinedocs/11.3.0/), coming from the base OS;
  - [CUDA 12.3](https://docs.nvidia.com/cuda/), coming from:
    https://developer.download.nvidia.com/compute/cuda/repos/rhel9/x86_64;
  - [ROCm 5.4.6](https://rocm.docs.amd.com/en/docs-5.4.3/), coming from:
    https://repo.radeon.com/rocm/rhel9/5.4.6/main;
  - [oneAPI 2023.2.1](https://www.intel.com/content/www/us/en/developer/tools/oneapi/overview.html),
    coming from: https://yum.repos.intel.com/oneapi;
  - [CMake 3.27.5](https://cmake.org/), coming from:
    https://cmake.org/files/v3.27;
  - [Ninja 1.11.1](https://ninja-build.org/), coming from:
    https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip;

## Starting The Container

Without much discussion, this is how I start the container on a machine that
has no GPU in it:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:alma9
```

In order to make use of an AMD GPU installed on the host, I execute the
following:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  --device /dev/kfd --privileged \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:alma9
```

In order to make use of an NVIDIA GPU installed on the host, I execute the
following:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  --gpus all \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:alma9
```

Naturally, the options can be combined in case you have multiple of these types
of GPUs available on your host system.

## Using the Container

On startup the container makes all compilers and tools available to the runtime
environment out of the box. However environment variables are largely missing
for making CMake use those compilers in a specific way. Especially in order to
use the oneAPI compilers, the user is expected to set `$CC`, `$CXX`,
`$SYCLCXX` and possibly `$CUDAHOSTCXX` appropriately by hand. (The setting for
`$SYCLCXX` and `$SYCLFLAGS` would depend on what backend(s) would be targeted by
a given build.)
